<?php

/**
 * Sudoku Board
 */
class Board
{
  var $rows = array(9);
  var $puzzle = array(9);//Saves Puzzle.
  function __construct($inputStr)//Create Board of input as given in example.
  {
    $input = $inputStr;
    $r = 0;
    foreach ($inputStr as $str) {
      $this->rows[$r] = explode(" ",$str);
      $this->puzzle[$r] = explode(" ",$str);
      $r++;
    }
  }
  public function showBoard($input = 1)//display the current state of the board.
  {
    $rows = $input;
    if($rows == 1)
    {
      $rows = $this->rows;
    }
    $r = 0;
    echo " __________________________\n|                          |\n";
    foreach ($rows as $row) {
      for ($i=0; $i < 9; $i++) {
        if($i%3 == 0)
        {
          echo "| ";
          if($i==0)
          {
            echo" ";
          }
        }
        echo $row[$i]." ";
        if($i == 8)
        {
          echo "  |";
        }

      }
      if($r%3 == 2 && $r!=8)
      {
        echo "\n|__________________________|\n|                          |\n";
      }
      elseif($r==8){
        echo "\n|__________________________|\n";
      }
      else echo "\n";
      $r++;
    }
  }
  public function getFixedSlots()//Gets puzzle fixed numbers.
  {
    $fixed = array();
    for ($r=0; $r < 9; $r++) {
      $row = $this->puzzle[$r];
      for ($c=0; $c < 9; $c++) {
        if($row[$c] != 0)
        {
          array_push($fixed,[$r,$c]);
        }
      }
    }
    return $fixed;
  }
  public function countDuplicatesInRows()//Score calculating function
  {
    $totalDuplicates = 0;
    foreach ($this->rows as $row) {
      $rowcount = array_fill(0,10,0);
      for ($i=0; $i < 9; $i++) {
        $rowcount[$row[$i]]++;
      }
      for ($i=1; $i < 10; $i++) {
        if($rowcount[$i] > 0)
        {
          $totalDuplicates += $rowcount[$i] - 1;
        }
      }
    }
    return floatval($totalDuplicates);
  }
  private function transpose($array) {//Helper Function to operate on rows while filling and swapping.
      $transpose = array();
      $r = 0;
      foreach ($this->rows as $row) {
        for ($i=0; $i < 9; $i++) {
          $transpose[$i][$r] = $row[$i];
        }
        $r++;
      }
      return $transpose;
  }
  public function showTransposedBoard()
  {
    $new = $this->transpose($this->rows);
    $this->showBoard($new);
  }
  public function fill()//Function that fills the board randomly.
  {
    $fixed = $this->getFixedSlots();
    $columns = $this->transpose($this->rows);
    for($c=0 ;$c<9 ;$c++) {
      $column = $columns[$c];
      $found = array();
      $filling = [1,2,3,4,5,6,7,8,9];
      //Gets all fixed numbers of puzzle to ensure they don't get overwritten
      for ($i=0; $i < count($fixed) ; $i++)
      {
        if($fixed[$i][1] == $c)
        {
          array_push($found,$this->rows[$fixed[$i][0]][$fixed[$i][1]]);
        }
      }
      foreach ($found as $number ) {
        $pos = array_search($number,$filling);
        array_splice($filling, $pos, 1);
      }
      $q = 0;
      for ($i=0; $i < 9; $i++) {
        if($column[$i] == 0)
        {
          $column[$i] = $filling[$q];
          $q++;
        }
      }
      $columns[$c] = $column;
    }
    $this->rows = $columns;
    $this->rows = $this->transpose($this->rows);
  }
  private function isFixed($r,$c)//Checks if number is fixed or not.
  {
    $fixed = $this->getFixedSlots();
    for ($i=0; $i < count($fixed); $i++) {
      if($fixed[$i][0] == $r && $fixed[$i][1] == $c)
      {
        return true;
      }
    }
    return false;
  }
  public function swapRandom()//Function that swaps two slots on the same column randomly.
  {
    $fixed = $this->getFixedSlots();
    $col = rand(0,8);
    $count = 0;
    //Making sure it does not try to swap in a column that have only 1 or less non fixed numbers.
    while($count >= 8)
    {
      $col = rand(0,8);
      $count = 0;
      for ($i=0; $i < count($fixed); $i++) {
        if($fixed[$i][1] == $col)
        {
          $count++;
        }
      }
    }
    //Making sure fixed numbers are not swapped.
    $first = rand(0,8);
    $second = rand(0,8);
    $Fxists = $this->isFixed($first,$col);
    $Sxists = $this->isFixed($second,$col);
    while($Fxists || $Sxists )
    {
      $first = rand(0,8);
      $second = rand(0,8);
      $Fxists = $this->isFixed($first,$col);
      $Sxists = $this->isFixed($second,$col);
    }


    $x = $this->rows[$first][$col];
    $this->rows[$first][$col] = $this->rows[$second][$col];
    $this->rows[$second][$col] = $x;
  }
}

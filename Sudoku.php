<?php

require_once('Board.php');

$input = [
"8 0 0 0 0 1 2 0 0",
"0 7 5 0 0 0 0 0 0",
"0 0 0 0 5 0 0 6 4",
"0 0 7 0 0 0 0 0 6",
"9 0 0 7 0 0 0 0 0",
"5 2 0 0 0 9 0 4 7",
"2 3 1 0 0 0 0 0 0",
"0 0 6 0 2 0 1 0 9",
"0 0 0 0 0 0 0 0 0"
];

$Board = new Board($input);//Model the Board.
$Board->showBoard();//Print Board.
echo "BOARD\n\n";
$Board->fill();
$time = (float)0.0;
$Temperature = 10;
$Board->showBoard();
echo "FILLED BOARD\n\n";
$i = 0;
$bestscore = [72,0];
$zeros = 0;
$bestBoard = new Board($input);
while(true)
{
  $i++;
  $Temperature -= 0.001;
  echo "Iteration : ".$i."\n";
  echo "Reached 0 : ".$zeros." Times\n";
  echo $Temperature."\n";
  $score = $Board->countDuplicatesInRows();
  if((float)$bestscore[0] > $score)
  {
    $bestscore = [$score,$i];
    $bestBoard = $Board;
  }
  echo "BEST : ".$bestscore[0]."    at Iteration : ".$bestscore[1]."\n";
  echo "SCORE: ".$score."\n";

  if($score == 0)
  {
    echo "SOLVED!!!\n";
    $Board->showBoard();
    return 1;
  }
  $random = (float)rand()/(float)getrandmax();
  $TEMP = $Board;
  $TEMP->swapRandom();
  $newScore = $TEMP->countDuplicatesInRows();
  $diff = (float)($newScore - $score);
  if($newScore < $score)
  {
    $Board = $TEMP;
    $Board->showBoard();
  }
  elseif($Temperature <= 0.001 && $score != 0)
  {
    $time = (float)0.0;
    $Temperature = 8;
    $zeros++;
    $Board = $bestBoard;
    $score = $Board->countDuplicatesInRows();
    $Board->fill();
    continue;
  }
  elseif($newScore > $score)
  {
    if($random <= exp(-($diff/$Temperature)))
    {
      $Board = $TEMP;
      $Board->showBoard();
      $time++;

    }
  }
}
